class Cursor(object):
    UP = '\033[{}A'
    DOWN = '\033[{}B'
    RIGHT = '\033[{}C'
    LEFT = '\033[{}D'
    HIDE = '\033[?25l'
    SHOW = '\033[?25h'
    PUSH = '\033[s'
    POP = '\033[u'
    CLEARLN = '\033[K'

    @classmethod
    def push(cls):
        print(cls.PUSH, end='')

    @classmethod
    def pop(cls):
        print(cls.POP, end='')

    @classmethod
    def up(cls, n=1):
        print(cls.UP.format(n), end='')

    @classmethod
    def down(cls, n=1):
        print(cls.DOWN.format(n), end='')

    @classmethod
    def left(cls, n=1):
        print(cls.LEFT.format(n), end='')

    @classmethod
    def right(cls, n=1):
        print(cls.RIGHT.format(n), end='')

    @classmethod
    def clearln(cls):
        print(cls.CLEARLN, end='')

    @classmethod
    def hide(cls, hidden=True):
        if not hidden:
            print(cls.SHOW, end='')
        else:
            print(cls.HIDE, end='')
