import copy
from clipy import Label


class Menu(object):
    EMPTY_CIRCLE = u'\u25EF'
    FILLED_CIRCLE = u'\u2B24'

    def __init__(self, title, menu=(), pos=1):
        self.title = title
        self.menu = menu
        self.current = pos

    def o(self, item):
        return Menu(self.title, self.menu + (item,))

    def show(self):
        bullet = Label(f'{self.EMPTY_CIRCLE} ').fcolor('green')
        print(self.title)
        for item in self.menu:
            print(f'{bullet} {item}')
        return self.menu[0]
