from clipy import __version__
from clipy import Label, Menu, Cursor, Console


def test_version():
    assert __version__ == '0.1.0'


def test_Label():
    lbl = Label('Hello World').fcolor('green')
    print(lbl)


if __name__ == '__main__':
    Console.echo(False)
    Cursor.hide()
    print('''
What is your favorite animal?
 - dog
 - cat
 - horse''')
    Cursor.up(3)
    char = Console.getkey()
    Cursor.down(3)
    Cursor.hide(False)
    Console.echo()
    print(repr(char))

    happy = (
        Menu(Label('How was your day?').style('bold'))
        .o('fine')
        .o('great')
        .o('terrible')
    ).show()
