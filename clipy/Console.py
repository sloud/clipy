import copy
import termios
import sys
import tty


class Console(object):

    @classmethod
    def echo(cls, visible=True):
        fd = sys.stdin.fileno()
        attr = TermiosAttr(fd)
        if visible:
            attr.lflag |= termios.ECHO
        else:
            attr.lflag &= ~termios.ECHO

        termios.tcsetattr(fd, termios.TCSANOW, attr.list())

    @classmethod
    def getkey(cls):
        fd = sys.stdin.fileno()
        oldST = TermiosAttr(fd)
        chars = []

        try:
            newST = oldST.raw()

            # block until at least one key is pressed
            chars.append(sys.stdin.read(1))

            # turn off blocking io
            newST.nblock().commit()
            chars.append(sys.stdin.read(4))

        finally:
            oldST.commit()
            return str(''.join(chars))


class TermiosAttr(object):
    def __init__(self, fd):
        self.fd = fd
        (self.iflag,
         self.oflag,
         self.cflag,
         self.lflag,
         self.ispeed,
         self.ospeed,
         self.cc
         ) = termios.tcgetattr(fd)

    def list(self):
        return [self.iflag, self.oflag, self.cflag, self.lflag,
                self.ispeed, self.ospeed, self.cc]

    def commit(self):
        termios.tcsetattr(self.fd, termios.TCSANOW, self.list())

    def nblock(self):
        attr = copy.deepcopy(self)
        attr.cc[termios.VTIME] = 0
        attr.cc[termios.VMIN] = 0
        return attr

    def echo(self, visible=True):
        attr = copy.deepcopy(self)
        if visible:
            attr.lflag |= termios.ECHO
        else:
            attr.lflag &= ~termios.ECHO
        return attr

    def raw(self):
        tty.setraw(self.fd)
        return TermiosAttr(self.fd)

    @classmethod
    def pair(cls, fd):
        old = cls(fd)
        new = copy.deepcopy(old)
        return (old, new)
