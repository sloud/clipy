import copy


class Label(object):
    """
    Creates styled string for terminal output using ansi escape characters. This will only
    work for console programs.
    """
    FCOLOR = {'black': 30, 'red': 31, 'green': 32, 'yellow': 33,
              'blue': 34, 'magenta': 35, 'cyan': 36, 'white': 37}

    BCOLOR = {'black': 40, 'red': 41, 'green': 42, 'yellow': 43,
              'blue': 44, 'magenta': 45, 'cyan': 46, 'white': 47}

    STYLE = {'normal': 0, 'bold': 1, 'uline': 4, 'blink': 5, 'reverse': 7}

    ESC = '\033['

    CLEAR = f'{ESC}{STYLE["normal"]};m'

    def __init__(self, label=None):
        self._label = label
        self._fcolor = None
        self._bcolor = None
        self._style = None

    def __str__(self):
        rep = self._header()
        rep.append(str(self._label))
        rep.extend(self.footer())
        return ''.join(rep)

    def __repr__(self):
        return f'"{self.__str__()}"'

    def label(self, label):
        nl = copy.deepcopy(self)
        nl._label = label
        return nl

    def fcolor(self, color):
        nl = copy.deepcopy(self)
        nl._fcolor = color
        return nl

    def bcolor(self, color):
        nl = copy.deepcopy(self)
        nl._bcolor = color
        return nl

    def style(self, style):
        nl = copy.deepcopy(self)
        nl._style = style
        return nl

    def strip(self):
        return copy.deepcopy(self._label)

    def is_formatted(self):
        return self._fcolor is not None or self._bcolor is not None or self._style is not None

    def header(self):
        return tuple(self._header)

    def _header(self):
        head = []
        if self.is_formatted():
            head.append(self.ESC)
            head.extend(self._encoding(self._fcolor, self.FCOLOR))
            head.extend(self._encoding(self._bcolor, self.BCOLOR))
            head.extend(self._encoding(self._style, self.STYLE))
            head[-1] = 'm'

        return head

    def footer(self):
        return tuple() if not self.is_formatted() else tuple(self.CLEAR)

    @classmethod
    def _encoding(cls, key, store):
        if key is not None:
            val = store.get(key)
            if val is not None:
                return [str(val), ';']
        return []
